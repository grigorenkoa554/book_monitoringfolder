﻿using System;
using System.IO;

namespace Book_MonitoringFolder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            FileSystemWatcher watcher = new FileSystemWatcher();
            try
            {
                watcher.Path = @".";
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            // 0 0 0 0 1 1
            watcher.NotifyFilter = NotifyFilters.LastAccess //  00100000
                | NotifyFilters.LastWrite//                     00010000
                | NotifyFilters.FileName//                      00000001
                | NotifyFilters.DirectoryName;//                00000010
            //                 watcher.NotifyFilter             00110011 - через енумы укaзываем, за чем мы хотим следить
            //      0001010101
            //      0001111101
            //или   0001111101
            //и     0001010101
            watcher.Filter = "*.txt"; //сделим только за txt
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);
            // Начать наблюдение за каталогом.
            watcher.EnableRaisingEvents = true;
            // Ожидать от пользователя команду завершения программы.
            Console.WriteLine(@"Press 'q' to quit app.");
            while (Console.Read() != 'q') ;
        }
        static void OnChanged(object source, FileSystemEventArgs e)
        {
            // Сообщить о действии изменения, создания или удаления файла.
            Console.WriteLine("File : {0} {1}!", e.FullPath, e.ChangeType);
        }
        static void OnRenamed(object source, RenamedEventArgs e)
        {
            // Сообщить о действии переименования файла.
            Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }
    }
}
